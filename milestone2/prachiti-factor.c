#include <stdio.h>
 
void Factors(int);  
 
int main()
{
  int No; 
   
  printf("\n Please Enter number to Find Factors\n");
  scanf("%d", &No);
 
  printf("\n Factors of a Number are:\n");
  Factors(No); 
 
  return 0;
}
 
void Factors(int No)
{ 
  int i; 
  
  for (i = 1; i <= No; i++)
   {
    if(No%i == 0)
     {
       printf("%d ", i);
     } 
   }
}