#include <stdio.h>
double FV(double rate, unsigned int nperiods, double PV);
int main()
{
	double rate; unsigned int nperiods; double PV;
	printf("Enter value of rate , nperiods , PV  :\n");
	scanf("%lf%u%lf",&rate,&nperiods,&PV);
	printf("FUTURE VALUE of an investment is %lf",FV(rate,nperiods,PV));
    return 0;
}
double FV(double rate, unsigned int nperiods, double PV)
{
	int i;
	double val=PV;
	//FV = PV * (1+rate)nperiods
	for(i=0;i<nperiods;i++)
		val=val*(1+rate);
	return val;
}